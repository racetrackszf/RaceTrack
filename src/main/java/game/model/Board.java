package game.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by szymo on 12.10.2015.
 */
public class Board {

    public static final int WIDTH = 17;
    public static final int HEIGHT = 30;

    private static TrackState board[][];

    public Board() {
        board = new TrackState[HEIGHT][WIDTH];
        for (int y = 0; y < HEIGHT; y++) {
            for (int x = 0; x < WIDTH; x++) {
                board[y][x] = TrackState.ROAD;
            }
        }
        prepareBoardOne();
    }

    public static List<Coordinate> startLine() {
        List<Coordinate> coordinates = new ArrayList();
        for (int y = 0; y < HEIGHT; y++) {
            for (int x = 0; x < WIDTH; x++) {
                if (board[y][x] == TrackState.START) {
                    coordinates.add(new Coordinate(x, y));
                }
            }
        }
        return coordinates;
    }

    public static List<Coordinate> finishLine() {
        List<Coordinate> coordinates = new ArrayList();
        for (int y = 0; y < HEIGHT; y++) {
            for (int x = 0; x < WIDTH; x++) {
                if (board[y][x] == TrackState.FINISH) {
                    coordinates.add(new Coordinate(x, y));
                }
            }
        }
        return coordinates;
    }

    public static List<Integer> yFinish() {
        List<Integer> yFinish = new ArrayList();
        for (Coordinate coordinate : finishLine()) {
            yFinish.add(coordinate.getY());
        }
        return yFinish;
    }

    public static TrackState[][] getBoard() {
        return board;
    }

    public static TrackState getState(int x, int y) {
        return board[y][x];
    }

    public static TrackState getState(State state) {
        return getState(state.getPositionX(), state.getPositionY());
    }

    //[columns][rows]
    public void prepareBoardOne() {

        ///UP LEFT corner
        for (int i = 0; i < 4; i++) {
            board[i][0] = TrackState.OFF_ROAD;
        }

        for (int i = 0; i < 3; i++) {
            board[i][1] = TrackState.OFF_ROAD;
        }

        board[0][2] = TrackState.OFF_ROAD;

        ///bottom left corner
        for (int y = HEIGHT - 16; y < HEIGHT; y++) {
            board[y][0] = TrackState.OFF_ROAD;
        }
        for (int y = HEIGHT - 10; y < HEIGHT; y++) {
            board[y][1] = TrackState.OFF_ROAD;
        }

        for (int y = HEIGHT - 3; y < HEIGHT; y++) {
            for (int x = 0; x < 3; x++) {
                board[y][x] = TrackState.OFF_ROAD;
            }
        }

        ///////bottom right corner
        for (int y = 7; y < HEIGHT; y++) {
            for (int x = WIDTH - 8; x < WIDTH; x++) {
                board[y][x] = TrackState.OFF_ROAD;
            }
        }
        board[7][WIDTH - 8] = TrackState.ROAD;


        ///////FINISH LINE////////
        for (int y = 0; y < 7; y++) {
            board[y][WIDTH - 1] = TrackState.FINISH;
        }

        ///////START LINE////////
        for (int x = 3; x < 9; x++) {
            board[HEIGHT - 1][x] = TrackState.START;
        }
    }

    public void printBoard() {
        for (int y = 0; y < HEIGHT; y++) {
            for (int x = 0; x < WIDTH; x++) {
                System.out.print(board[y][x].getReward() == -5 ? "  " : board[y][x].getReward());
            }
            System.out.println();
        }
    }
}
