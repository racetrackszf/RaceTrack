package game.model;

/**
 * Created by szymo on 14.10.2015.
 */
public enum TrackState {
    START(-5),
    FINISH(0),
    ROAD(-1),
    OFF_ROAD(-5);

    private final int reward;

    TrackState(int reward) {
        this.reward = reward;
    }

    public int getReward() {
        return reward;
    }
}
