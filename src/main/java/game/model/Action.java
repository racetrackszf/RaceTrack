package game.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by szymo on 15.10.2015.
 */
public enum Action {
    N(0, 0, -1),
    S(1, 0, +1),
    E(2, -1, 0),
    W(3, +1, 0),

    NE(4, -1, -1),
    NW(5, +1, -1),
    SE(6, -1, +1),
    SW(7, +1, +1);

    //STAY(0,0) //action not included

    private final int number;
    private final int x;
    private final int y;

    Action(int number, int x, int y) {
        this.number = number;
        this.x = x;
        this.y = y;
    }

    public Coordinate getValues() {
        return new Coordinate(x, y);
    }

    public int getNumber(){
        return number;
    }
    public static  List<Integer>  getNumbers(){
        List<Integer> actions = new ArrayList();
        actions.add(N.getNumber());
        actions.add(S.getNumber());
        actions.add(E.getNumber());
        actions.add(W.getNumber());

        actions.add(NE.getNumber());
        actions.add(NW.getNumber());
        actions.add(SE.getNumber());
        actions.add(SW.getNumber());
        return actions;
    }

    public static List<Coordinate> getActions() {
        List<Coordinate> actions = new ArrayList();
        actions.add(N.getValues());
        actions.add(S.getValues());
        actions.add(E.getValues());
        actions.add(W.getValues());

        actions.add(NE.getValues());
        actions.add(NW.getValues());
        actions.add(SE.getValues());
        actions.add(SW.getValues());
        return actions;
    }

}
