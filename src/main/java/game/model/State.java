package game.model;

/**
 * Created by fix92 on 14.10.2015.
 */
public class State {

    private int xPos;    // x coordinate of state
    private int yPos;    // y coordinate of state
    private int xVelocity;    // velocity in x direction
    private int yVelocity;    // velocity in y direction

    public double greedyProb = 0.5;

    public State(int xPos, int yPos, int xVelocity, int yVelocity) {
        this.xPos = xPos;
        this.yPos = yPos;
        this.xVelocity = xVelocity;
        this.yVelocity = yVelocity;
    }

    public State act(Coordinate coordinate) {
        State newState = copy();

        newState.setVelocityX(newState.getVelocityX() + coordinate.getX());
        newState.setVelocityY(newState.getVelocityY() + coordinate.getY());

        newState.setPositionX(newState.getPositionX() + newState.getVelocityX());
        newState.setPositionY(newState.getPositionY() + newState.getVelocityY());

        return newState;
    }

    private State copy() {
        return new State(xPos, yPos, xVelocity, yVelocity);
    }

    public State stochasticMovements(Coordinate coordinate) {

        State newState = act(coordinate);

        double rand = Math.random();
        if (rand < greedyProb) {
            if (Math.random() < 0.5) {
                newState.setPositionX(newState.getPositionX() + 1);
            } else {
                newState.setPositionY(newState.getPositionY() - 1);
            }
        }

        return newState;
    }

    public int getPositionX() {
        return xPos;
    }

    public void setPositionX(int xPos) {
        this.xPos = xPos;
    }

    public int getPositionY() {
        return yPos;
    }

    public void setPositionY(int yPos) {
        this.yPos = yPos;
    }

    public int getVelocityX() {
        return xVelocity;
    }

    public void setVelocityX(int xVelocity) {
        this.xVelocity = xVelocity;
    }

    public int getVelocityY() {
        return yVelocity;
    }

    public void setVelocityY(int ySpeed) {
        this.yVelocity = ySpeed;
    }

    @Override
    public String toString() {
        return "State{" +
                "xPos=" + xPos +
                ", yPos=" + yPos +
                ", xVelocity=" + xVelocity +
                ", yVelocity=" + yVelocity +
                '}';
    }
}