package game;

import game.model.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by szymo on 12.10.2015.
 * http://artint.info/html/ArtInt_227.html
 */
public class ValueIteration {

    //When the car is sliding it is sliding either 1 cell up or 1 cell to the right
    // (both with equal probability)
    private static final double PROBABILITY_OF_EXPECTED_MOVE = 0.5;
    private static final double PROBABILITY_OF_STOCHASTIC_MOVE = 0.25;

    private static final int MIN_VELOCITY = -5;
    private static final int MAX_VELOCITY = 5;

    private static final int FINISH_REWARD = TrackState.FINISH.getReward();
    private static final double MAX_ERROR = 1e-4;
    private static final int MAX_ITERATIONS = 10;
    private double value[][][][];

    public ValueIteration() {

        this.value = new double[Board.HEIGHT][Board.WIDTH][MAX_VELOCITY * 2 + 1][MAX_VELOCITY * 2 + 1];

        setBoardValuesBasedOnDistanceFromFinishLine();
        updateBoardValuesBasedOnRewards();
    }

    /**
     * Set board values depends on distance from finish line.
     * Finish point = 10
     * Other fields have a minus value.
     */
    private void setBoardValuesBasedOnDistanceFromFinishLine() {

        List<Coordinate> finishLine = Board.finishLine();
        for (int y = 0; y < Board.HEIGHT; y++) {
            for (int x = 0; x < Board.WIDTH; x++) {
                for (int velY = MIN_VELOCITY; velY <= MAX_VELOCITY; velY++) {
                    for (int velX = MIN_VELOCITY; velX <= MAX_VELOCITY; velX++) {
                        distanceFromFinishLine(new State(x, y, velX, velY), finishLine);
                    }
                }
            }
        }
    }

    private void distanceFromFinishLine(State state, List<Coordinate> finishLine) {
        double minimum = Board.HEIGHT + Board.WIDTH;
        double valueNow;
        for (Coordinate coordinate : finishLine) {
            valueNow = -Math.sqrt(Math.pow(state.getPositionX() - coordinate.getX(), 2)
                    + Math.pow(state.getPositionY() - coordinate.getY(), 2));
            if (minimum > valueNow) {
                minimum = valueNow;
            }

        }
        setValue(state, minimum);
    }

    /**
     * update board values based on reward data and calculate error
     * OUT OF ARRAY -1000
     */
    private void updateBoardValuesBasedOnRewards() {
        double error;
        int i = 0;
        do {
            error = 0;
            for (int y = 0; y < Board.HEIGHT; y++) {
                for (int x = 0; x < Board.WIDTH; x++) {
                    for (int velY = MIN_VELOCITY; velY <= MAX_VELOCITY; velY++) {
                        for (int velX = MIN_VELOCITY; velX <= MAX_VELOCITY; velX++) {
                            State state = new State(x, y, velX, velY);
                            double value = getValue(state);
                            double rew = possibleReward(state).getFirst();
                            double difference = Math.abs(value - rew);
                            setValue(state, rew);
                            error = Math.max(error, difference);
                        }
                    }
                }
            }
            i++;
            System.out.println("Iteration: " + i + " error: " + error);
        } while (error > MAX_ERROR || i < MAX_ITERATIONS);

    }


    private Pair<Double, Coordinate> possibleReward(State state) {
        double maxValue = -1;
        Coordinate bestAction = new Coordinate(0, 0);
        for (Coordinate coordinate : possibleActions(state)) {

            State stateExpected = state.act(coordinate);

            State stochasticStateOne = state.act(coordinate);
            stochasticStateOne.setPositionX(stochasticStateOne.getPositionX() + 1);

            State stochasticStateTwo = state.act(coordinate);
            stochasticStateTwo.setPositionY(stochasticStateOne.getPositionY() - 1);

            double sum = 0;
            sum += PROBABILITY_OF_EXPECTED_MOVE * getReward(stateExpected);
            sum += PROBABILITY_OF_STOCHASTIC_MOVE * getReward(stochasticStateOne);
            sum += PROBABILITY_OF_STOCHASTIC_MOVE * getReward(stochasticStateTwo);

            if (maxValue == -1 || sum > maxValue) {
                maxValue = sum;
                bestAction = coordinate;
            }

        }
        return new Pair(maxValue, bestAction);
    }

    public Episode generateEpisode(State state) {
        Episode episode = new Episode(state);
        Coordinate action;

        int reward = 0;
        do {
            action = possibleReward(state).getSecond();
            state = state.stochasticMovements(action);
            episode.add(action, state);
            reward += Board.getState(state).getReward();
            System.out.println(state);
        } while (state.getPositionX() < Board.WIDTH - 1 && !Board.yFinish().contains(state.getPositionY()));

        System.out.println(reward);
        episode.print();
        return episode;
    }

    private double getValue(State state) {
        return this.value[state.getPositionY()][state.getPositionX()]
                [state.getVelocityY() + MAX_VELOCITY][state.getVelocityX() + MAX_VELOCITY];
    }

    private void setValue(State state, Double number) {
        this.value[state.getPositionY()][state.getPositionX()]
                [state.getVelocityY() + MAX_VELOCITY][state.getVelocityX() + MAX_VELOCITY] = number;
    }

    public double getReward(State state) {
        int x = state.getPositionX();
        int y = state.getPositionY();
        if (x < 0 || y < 0 || y >= Board.HEIGHT) {
            return -10;
        } else if (x >= Board.WIDTH && Board.yFinish().contains(y)) {
            return FINISH_REWARD;
        } else if (x >= Board.WIDTH && !Board.yFinish().contains(y)) {
            return -10;
        } else {
            return Board.getState(state).getReward() + getValue(state);
        }
    }

    private List<Coordinate> possibleActions(State state) {
        List<Coordinate> possibleActions = new ArrayList();

        for (Coordinate coordinate : Action.getActions()) {
            int newVelX = coordinate.getX() + state.getVelocityX();
            int newVelY = coordinate.getY() + state.getVelocityY();

            if (newVelX >= MIN_VELOCITY && newVelX <= MAX_VELOCITY
                    && newVelY >= MIN_VELOCITY && newVelY <= MAX_VELOCITY) {
                possibleActions.add(coordinate);
            }
        }
        return possibleActions;
    }


}
