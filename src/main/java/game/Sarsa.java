package game;

import game.model.Action;
import game.model.Board;
import game.model.State;

/**
 * Created by szymo on 10/19/2015.
 */
public class Sarsa {

    private static final double GREEDY_PROBABILITY = 0.5;

    private static final int MIN_VELOCITY = -5;
    private static final int MAX_VELOCITY = 5;
    private static final int NUMBER_OF_ACTIONS = Action.getActions().size();

    private static final double DISCOUNT = 0.9;
    private static final double ALPHA = 0.4;
    private static final double LAMBDA = 0.9;

    //The Q values: Q[y, x, velY, velX, action]
    public double qValues[][][][][];

    //Eligibility trace for state (y,x, velY, velX) and actions
    public double e[][][][][];

    public boolean tracing = false;

    public int prevX = 0;
    public int prevY = 0;
    public int prevVelX = 0;
    public int prevVelY = 0;
    public int prevAction = 0;
    public double reward = 0;
    int totalReward = 0;
    int numberOfSteps = 0;

    public boolean replacingTrace = false;


    public Sarsa() {

        qValues = new double[Board.HEIGHT][Board.WIDTH][MAX_VELOCITY * 2 + 1][MAX_VELOCITY * 2 + 1][NUMBER_OF_ACTIONS];
        e = new double[Board.HEIGHT][Board.WIDTH][MAX_VELOCITY * 2 + 1][MAX_VELOCITY * 2 + 1][NUMBER_OF_ACTIONS];

        initValues();
    }

    private void initValues() {
        for (int y = 0; y < Board.HEIGHT; y++) {
            for (int x = 0; x < Board.WIDTH; x++) {
                for (int velY = MIN_VELOCITY; velY <= MAX_VELOCITY; velY++) {
                    for (int velX = MIN_VELOCITY; velX <= MAX_VELOCITY; velX++) {
                        for (int a = 0; a < NUMBER_OF_ACTIONS; a++) {
                            qValues[y][x][velY + MAX_VELOCITY][velX + MAX_VELOCITY][a] = 0.0;
                            e[y][x][velY + MAX_VELOCITY][velX + MAX_VELOCITY][a] = 0.0;
                        }
                    }
                }
            }
        }
    }

    public void doSteps(int count, State state) {
        for (int i = 0; i < count; i++) {
            if (Math.random() < GREEDY_PROBABILITY) {
                int startDir = (int) (Math.random() * NUMBER_OF_ACTIONS);
                double bestVal = qValues[state.getPositionY()][state.getPositionX()]
                        [state.getVelocityY() + MAX_VELOCITY][state.getVelocityX() + MAX_VELOCITY]
                        [Action.getNumbers().get(startDir)];
                int bestDir = startDir;
                for (int dir = 1; dir < NUMBER_OF_ACTIONS; dir++) {
                    startDir = (startDir + 1) % NUMBER_OF_ACTIONS;
                    if (qValues[state.getPositionY()][state.getPositionX()]
                            [state.getVelocityY() + MAX_VELOCITY][state.getVelocityX() + MAX_VELOCITY]
                            [Action.getNumbers().get(startDir)] > bestVal) {

                        bestVal = qValues[state.getPositionY()][state.getPositionX()]
                                [state.getVelocityY() + MAX_VELOCITY][state.getVelocityX() + MAX_VELOCITY]
                                [Action.getNumbers().get(startDir)];

                        bestDir = startDir;
                    }
                }
                doStep(bestDir, state);
            } else {
                if (Math.random() < 0.5) {
                    doStep(Action.getNumbers().get(0), state); //N
                } else {
                    doStep(Action.getNumbers().get(3), state); //E
                }
            }
        }
        System.out.println(numberOfSteps);
    }


    public void doStep(int nextAction, State state) {

        double newDatum = reward + DISCOUNT * qValues[state.getPositionY()][state.getPositionX()]
                [state.getVelocityY()][state.getVelocityX()][Action.getNumbers().get(nextAction)];

        double delta = newDatum - qValues[prevY][prevX][prevVelY + MAX_VELOCITY][prevVelX + MAX_VELOCITY][prevAction];
        if (replacingTrace) // replacing trace
        {
            for (int action = 0; action < NUMBER_OF_ACTIONS; action++) {
                if (action != prevAction)
                    e[prevY][prevX][prevVelY + MAX_VELOCITY][prevVelX + MAX_VELOCITY][action] = 0.0;
            }
            e[prevY][prevX][prevVelY + MAX_VELOCITY][prevVelX + MAX_VELOCITY][prevAction] = 1.0;
        } else {
            e[prevY][prevX][prevVelY + MAX_VELOCITY][prevVelX + MAX_VELOCITY][prevAction]++;
        }

        double alphaDelta = ALPHA * delta;
        double gammaLambda = DISCOUNT * LAMBDA;

        for (int y = 0; y < Board.HEIGHT; y++) {
            for (int x = 0; x < Board.WIDTH; x++) {
                for (int velY = MIN_VELOCITY; velY <= MAX_VELOCITY; velY++) {
                    for (int velX = MIN_VELOCITY; velX <= MAX_VELOCITY; velX++) {
                        for (int a = 0; a < NUMBER_OF_ACTIONS; a++) {
                            qValues[y][x][velY + MAX_VELOCITY][velX + MAX_VELOCITY][a] =
                                    alphaDelta * e[y][x][velY + MAX_VELOCITY][velX + MAX_VELOCITY][a];
                            e[y][x][velY + MAX_VELOCITY][velX + MAX_VELOCITY][a] *= gammaLambda;
                        }
                    }
                }
            }
        }

        // Store state and action
        prevX = state.getPositionX();
        prevY = state.getPositionY();
        prevAction = nextAction;

        // do the action and store the reward
        reward = doBoardStep(nextAction, state);
    }

    public double doBoardStep(int action, State state) {
        double reward;

        int x = state.getPositionX();
        int y = state.getPositionY();
        if (x < 0 || y < 0 || y >= Board.HEIGHT) {
            reward = -10;
        } else if (x >= Board.WIDTH && Board.yFinish().contains(y)) {
            reward = 0;
        } else if (x >= Board.WIDTH && !Board.yFinish().contains(y)) {
            reward = -10;
        } else {
            reward = Board.getState(state).getReward();
        }
        state.act(Action.getActions().get(action));

        numberOfSteps++;
        totalReward += reward;

        return reward;
    }


}


