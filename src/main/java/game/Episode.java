package game;

import game.model.Board;
import game.model.Coordinate;
import game.model.State;
import game.model.TrackState;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by szymo on 15.10.2015.
 */
public class Episode {

    public List<Coordinate> actions;
    public List<State> states;
    private State state;

    public Episode() {
        this.actions = new ArrayList();
        this.states = new ArrayList();
    }

    public Episode(State state) {
        this();
        this.state = state;
    }

    public void add(Coordinate action, State state) {
        actions.add(action);
        states.add(state);
    }

    public void add(State state) {
        states.add(state);
    }

    @Override
    public String toString() {
        return "Episode{" +
                "state=" + state +
                '}';
    }


    public void print() {
        TrackState[][] trackState = Board.getBoard();
        String value = "";

        for (int y = 0; y < Board.HEIGHT; y++) {
            for (int x = 0; x < Board.WIDTH; x++) {
                for (State state : states) {
                    if (state.getPositionX() == x && state.getPositionY() == y) {
                        value = "XX";
                    }
                }
                if (!value.equals("XX")) {
                    value = String.valueOf(trackState[y][x].getReward());
                }
                System.out.print(value);
                value = "";
            }
            System.out.println();
        }

    }

}
