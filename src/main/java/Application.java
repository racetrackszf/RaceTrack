
import game.Sarsa;
import game.ValueIteration;
import game.model.Board;
import game.model.Coordinate;
import game.model.State;

import java.util.Random;

/**
 * Created by szymo on 12.10.2015.
 */
public class Application {

    public static void main(String[] args) {

        Board board = new Board();
        board.printBoard();

        Coordinate startPoint = Board.startLine().get(new Random().nextInt(Board.startLine().size()));
        State startState = new State(startPoint.getX(), startPoint.getY(), 0, 0);

        Sarsa sarsa = new Sarsa();
        sarsa.doSteps(100, startState);

        ValueIteration mc = new ValueIteration();
        mc.generateEpisode(startState);


        // For all possible start points
        /*
        for(int i = 0; i < Board.startLine().size(); i++){
            startPoint = Board.startLine().get(i);
            startState = new State(startPoint.getX(), startPoint.getY(), 0, 0);
            System.out.println("STARTPOINT " + i);
            mc.generateEpisode(startState);
        }
        */

    }


}
