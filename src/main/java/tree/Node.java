package tree;

import java.util.List;

/**
 * Created by szymo on 12.10.2015.
 */
public class Node<T> {

    private T data;
    private Node<T> parent;
    private List<Node<T>> children;

    public Node(T data) {
        this.data = data;
    }

    public Node(T data, Node<T> parent) {
        this.data = data;
        this.parent = parent;
    }

    public Node(T data, Node<T> parent, List<Node<T>> children) {
        this.data = data;
        this.parent = parent;
        this.children = children;
    }

    public T getData() {
        return data;
    }

    public Node<T> getParent() {
        return parent;
    }

    public List<Node<T>> getChildren() {
        return children;
    }

    public void addChildren(Node<T> children) {
        this.children.add(children);
    }

    public void addChildrens(List<Node<T>> childrenList) {
        this.children.addAll(childrenList);
    }

    public boolean isLeaf() {
        return getChildren().size() == 0 ? true : false;
    }

    public void makeRoot() {
        this.parent = null;
    }

    @Override
    public String toString() {
        return "Node{" +
                "data=" + data +
                ", parent=" + parent +
                ", children=" + children +
                '}';
    }
}
